package com.appdirect.integration.restapi;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.integration.config.ViewMapper;
import com.appdirect.integration.domain.Account;
import com.appdirect.integration.restapi.view.AccountVO;
import com.appdirect.integration.service.AccountService;

@RestController
@RequestMapping({"/api/v1",""})
public class AccountController {

	private static final Logger logger = LogManager.getLogger(AccountController.class);

	@Autowired
	private AccountService accountService;

	@Autowired
	private ViewMapper<AccountVO> accountVOViewMapper;

	@Autowired
	private ViewMapper<Account> accountViewMapper;

	/**
	 * Rest API for integration testing for other APIs for Account We will not
	 * create an account from this API
	 * 
	 * @return
	 */
	@RequestMapping(value = "/account/create", method = RequestMethod.POST)
	public @ResponseBody Account createAccount(@RequestBody AccountVO accountVO) {
		logger.info("Creating an account ....");

		Account account = accountViewMapper.map(accountVO, Account.class);
		Account savedAccount = accountService.saveAccount(account);
		return savedAccount;

	}

	/**
	 * Get all the accounts available in the system
	 * 
	 * @return
	 */
	@RequestMapping(value = "/accounts", method = RequestMethod.GET)
	public @ResponseBody List<AccountVO> getAllAccount() {
		logger.info("Finding all the accounts in the system ...");
		List<Account> accounts = accountService.getAllAccount();
		List<AccountVO> accountVOList = new ArrayList<AccountVO>();
		for (Account account : accounts) {
			accountVOList.add(accountVOViewMapper.map(account, AccountVO.class));
		}

		return accountVOList;
	}
}
