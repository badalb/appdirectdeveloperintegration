package com.appdirect.integration.restapi;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	private static final Logger logger = LogManager.getLogger(UserController.class);

	@Value("${event.url}")
	private String eventURL;

	@Value("${login.url}")
	private String loginURL;

	@Value("${logout.url}")
	private String logoutURL;

	@Value("${user.list}")
	private String userList;

	@Value("${account.list}")
	private String accountList;

	/**
	 * User home after login successful
	 * @param model
	 * @return
	 */
	@RequestMapping("/apidetail")
	public String apiList(Model model) {

		logger.info("Login Successful ...... ");
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("user", user.getUsername());
		model.addAttribute("SubscriptionURL", eventURL);
		model.addAttribute("LoginURL", loginURL);
		model.addAttribute("LogoutURL", logoutURL);

		model.addAttribute("UserListURL", userList);
		model.addAttribute("AccountList", accountList);

		return "apidetail";
	}
}
