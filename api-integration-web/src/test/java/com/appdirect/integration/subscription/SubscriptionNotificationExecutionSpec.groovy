package com.appdirect.integration.subscription

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration

import spock.lang.Specification
import spock.lang.Stepwise

import com.appdirect.Application
import com.appdirect.integration.handler.IntegrationEventHandler
import com.appdirect.integration.vo.EventInfo
import com.google.gson.Gson

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes
	= [Application.class])@WebAppConfiguration
	@IntegrationTest("server.port:8000")
	@Stepwise
class SubscriptionNotificationExecutionSpec extends Specification {

	@Value('${local.server.port}')
	int port;

	@Autowired
	IntegrationEventHandler integrationEventHandler;

	 def " should cancel subscription" (){
	 setup:
	
	 def jsonStr ="{\"type\":\"SUBSCRIPTION_NOTICE\",\"marketplace\":{\"partner\":\"ACME\",\"baseUrl\":\"https://acme.appdirect.com\"},\"applicationUuid\":null,\"flag\":\"STATELESS\",\"creator\":null,\"payload\":{\"user\":null,\"company\":null,\"account\":{\"accountIdentifier\":\"8835e97c-ce3d-411c-83db-21c1060a77ac\",\"status\":\"SUSPENDED\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":{\"type\":\"DEACTIVATED\",\"message\":null},\"configuration\":{}},\"returnUrl\":null,\"links\":[]}"
	 def gson = new Gson()
	 def event = gson.fromJson(jsonStr, EventInfo.class)
	
	 when:
	
	 def apiResponse = integrationEventHandler.handleEvent(event)
	
	 then:
	 apiResponse.accountIdentifier != null
	 }
}


