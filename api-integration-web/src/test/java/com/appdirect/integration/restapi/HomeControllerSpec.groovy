package com.appdirect.integration.restapi

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

import spock.lang.Specification
import spock.lang.Stepwise

import com.appdirect.Application

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes
= [Application.class])@WebAppConfiguration
@IntegrationTest("server.port:8000")
@Stepwise
class HomeControllerSpec extends Specification {
	
	@Value('${local.server.port}')
	int port;

	void"should return homepage for appdirect challenge"() {
		
		setup:
		def restTemplate = new RestTemplate()
		
		when:
	   def url = "http://localhost:$port";
	   def response = restTemplate.getForObject(url, HashMap.class)
	   
		then:
	 
		response.get("Subscription URL :") == 'https://secret-falls-92557.herokuapp.com/appdirect/api/v1/event?eventUrl={eventUrl}&token={token}'
	}
}
