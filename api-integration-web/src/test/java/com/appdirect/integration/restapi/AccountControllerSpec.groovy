package com.appdirect.integration.restapi

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

import spock.lang.Specification
import spock.lang.Stepwise

import com.appdirect.Application
import com.appdirect.integration.domain.Account
import com.appdirect.integration.restapi.view.AccountVO

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes
= [Application.class])@WebAppConfiguration
@IntegrationTest("server.port:8000")
@Stepwise
class AccountControllerSpec extends Specification{

	@Value('${local.server.port}')
	int port;


	void"should save an account"() {

		setup:
		def restTemplate = new RestTemplate()

		def accountVO = new AccountVO()
		accountVO.uuid='563b56b1-ff3d-4c48-adcf-5af0cbc3cc69'
		accountVO.appDirectBaseUrl='https://www.acme.appdirecttest.com'
		accountVO.editionCode='FREE'
		accountVO.maxUsers=2
		accountVO.isActive=1
		when:

		String uri="http://localhost:$port/api/v1/account/create";
		def account=restTemplate.postForObject(uri, accountVO, Account.class);

		then:
		account.uuid == accountVO.uuid
	}


	void"should return list of accounts"() {

		setup:
		def restTemplate = new RestTemplate()

		when:
		def url = "http://localhost:$port/api/v1/accounts";
		def response = restTemplate.getForObject(url, ArrayList.class)
		def respObj = response[0]
		then:

		respObj.uuid == '563b56b1-ff3d-4c48-adcf-5af0cbc3cc69'
	}
}
