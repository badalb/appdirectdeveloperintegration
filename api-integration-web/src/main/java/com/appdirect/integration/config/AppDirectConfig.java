package com.appdirect.integration.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppDirectConfig {

	@Value("${consumer.key}")
	private String consumerKey;

	@Value("${consumer.secret}")
	private String secret;

	@Bean
	public AppDirectClientSecret appDirectClient() {
		return new AppDirectClientSecret(consumerKey, secret);
	}
}
