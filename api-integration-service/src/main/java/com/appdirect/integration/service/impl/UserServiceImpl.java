package com.appdirect.integration.service.impl;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.appdirect.integration.domain.User;
import com.appdirect.integration.repository.UserRepository;
import com.appdirect.integration.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional(readOnly = true)
	public User findUserByUuid(String uuid) {
		logger.info("Fetching user for UUID: "+ uuid);
		return userRepository.findByUuid(uuid);
	}

	@Override
	@Transactional(readOnly = true)
	public User findUserByOpenId(String openId) {
		logger.info("Fetching user for ID: "+ openId);
		return userRepository.findByOpenId(openId);
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<User> findAllUsers() {
		logger.info("Fetching all users ..");
		return (List<User>) userRepository.findAll();
	}


}
