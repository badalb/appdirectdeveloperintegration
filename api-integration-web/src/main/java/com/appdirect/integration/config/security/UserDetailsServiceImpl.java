package com.appdirect.integration.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.openid.OpenIDAuthenticationToken;
import org.springframework.stereotype.Component;

import com.appdirect.integration.service.UserService;

import java.util.Collections;

@Component
public class UserDetailsServiceImpl
		implements UserDetailsService, AuthenticationUserDetailsService<OpenIDAuthenticationToken> {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String openId) {
		//get user details from database
		com.appdirect.integration.domain.User user = userService.findUserByOpenId(openId);
		if (user != null) {

			return new User(user.getFirstName() + " " + user.getLastName(), "",
					Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
		}
		//if not at least user is authenticated from appDirect so allow access
		return new User(openId, "", Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
	}

	@Override
	public UserDetails loadUserDetails(OpenIDAuthenticationToken token) {
		return loadUserByUsername((String) token.getPrincipal());
	}

}