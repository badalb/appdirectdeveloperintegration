package com.appdirect.integration.service.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.appdirect.integration.domain.User;
import com.appdirect.integration.repository.UserRepository;
import com.appdirect.integration.service.builder.TestUserBuilder;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private UserServiceImpl userServiceImpl;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindUserByUuid() {
		User user = TestUserBuilder.buildTestUser();
		String uuid = user.getUuid();

		when(userServiceImpl.findUserByUuid(uuid)).thenReturn(user);

		User result = userServiceImpl.findUserByUuid(uuid);

		verify(userRepository, times(1)).findByUuid(uuid);

		// API invocation result & data validation
		Assert.assertTrue(result != null);
		Assert.assertEquals(result.getUuid(), uuid);

	}

	@Test
	public void testFindUserByOpenId() {
		User user = TestUserBuilder.buildTestUser();
		String openId = user.getOpenId();

		when(userServiceImpl.findUserByOpenId(openId)).thenReturn(user);

		User result = userServiceImpl.findUserByOpenId(openId);

		verify(userRepository, times(1)).findByOpenId(openId);

		// API invocation result & data validation
		Assert.assertTrue(result != null);
		Assert.assertEquals(result.getOpenId(), openId);
	}

	@Test
	public void testFindAllUsers() {

		User user = TestUserBuilder.buildTestUser();

		List<User> users = new ArrayList<>();
		users.add(user);

		when(userServiceImpl.findAllUsers()).thenReturn(users);

		List<User> resultList = userServiceImpl.findAllUsers();

		// API was called exactly once
		verify(userRepository, times(1)).findAll();
		// API invocation result & data validation
		Assert.assertTrue(resultList != null);
		Assert.assertEquals(resultList.get(0).getEmail(), "appDirectTest@test.com");
	}

}
