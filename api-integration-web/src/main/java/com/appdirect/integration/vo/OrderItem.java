package com.appdirect.integration.vo;

import com.appdirect.integration.vo.type.PriceUnit;

public class OrderItem {

	private Integer quantity;

	private PriceUnit unit;

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public PriceUnit getUnit() {
		return unit;
	}

	public void setUnit(PriceUnit unit) {
		this.unit = unit;
	}

}
