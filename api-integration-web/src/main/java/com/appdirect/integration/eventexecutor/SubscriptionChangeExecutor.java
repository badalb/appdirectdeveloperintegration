package com.appdirect.integration.eventexecutor;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.appdirect.integration.domain.Account;
import com.appdirect.integration.domain.OrderLineItem;
import com.appdirect.integration.domain.SubscriptionOrder;
import com.appdirect.integration.service.AccountService;
import com.appdirect.integration.validator.Validator;
import com.appdirect.integration.vo.APIResponse;
import com.appdirect.integration.vo.EventInfo;
import com.appdirect.integration.vo.type.ErrorCode;

@Component("subsChangeExecutor")
public class SubscriptionChangeExecutor implements EventExecutor {

	private static final Logger logger = LogManager.getLogger(SubscriptionChangeExecutor.class);

	@Autowired
	private AccountService accountService;
	
	@Autowired
	@Qualifier("objectExistsValidator")
	private Validator objectExistsValidator;

	/**
	 * Handle subscription change event
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public APIResponse executeEvent(EventInfo eventInfo) {

		String accountId = eventInfo.getPayload().getAccount().getAccountIdentifier();
		logger.info("Processing subscription change event for account :" + accountId);

		Account account = accountService.getAccountById(accountId);
		
		if(!objectExistsValidator.validate(account)){
			return new APIResponse(false, "Account does not exists for account ID: " + accountId, ErrorCode.ACCOUNT_NOT_FOUND);
		}

		SubscriptionOrder order = new SubscriptionOrder();

		List<OrderLineItem> lineItems = new ArrayList<>();

		try {

			BeanUtils.copyProperties(order, eventInfo.getPayload().getOrder());

			for (com.appdirect.integration.vo.OrderItem item : eventInfo.getPayload().getOrder().getItems()) {
				OrderLineItem orderItemTosave = new OrderLineItem();
				BeanUtils.copyProperties(orderItemTosave, item);
				orderItemTosave.setOrder(order);
				lineItems.add(orderItemTosave);
			}

		} catch (IllegalAccessException e) {
			logger.debug("Error processing bean data for subscription change please take care. Proceeding with execution now for account: "+accountId);
		} catch (InvocationTargetException e) {
			logger.debug("Error processing bean data for subscription change please take care. Proceeding with execution now for account: "+accountId);
		}
		// set order and order item
		List<SubscriptionOrder> orders = new ArrayList<>();

		order.setAccount(account);
		order.setOrderLineItem(lineItems);
		orders.add(order);
		
		account.getOrder().clear();
		account.getOrder().addAll(orders);

		accountService.saveAccount(account);
		return new APIResponse(true, "Subscription changed for account ID: " + accountId, null, accountId);

	}

}
