package com.appdirect.integration.vo.type;

public enum EventFlag {
	STATELESS, DEVELOPMENT;
}
