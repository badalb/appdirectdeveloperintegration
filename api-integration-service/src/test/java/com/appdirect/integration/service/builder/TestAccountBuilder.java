package com.appdirect.integration.service.builder;

import com.appdirect.integration.domain.Account;

public class TestAccountBuilder {

	public static Account buldAccount() {
		Account account = new Account();
		account.setId("ec5d8eda-5cec-444d-9e30-125b6e4b67e2");
		account.setAppDirectBaseUrl("https://acme.appdirect.com");
		account.setIsActive(1);
		account.setAccountIdentifier("ec5d8eda-5cec-444d-9e30-125b6e4b67e2");

		return account;
	}

}
