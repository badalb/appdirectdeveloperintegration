package com.appdirect.integration.service;

import java.util.List;

import com.appdirect.integration.domain.User;

public interface UserService {

	public User findUserByUuid(String uuid);

	public User findUserByOpenId(String openId);

	public List<User> findAllUsers();


}
