package com.appdirect.integration.eventexecutor;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.appdirect.integration.domain.Account;
import com.appdirect.integration.service.AccountService;
import com.appdirect.integration.validator.Validator;
import com.appdirect.integration.vo.APIResponse;
import com.appdirect.integration.vo.EventInfo;
import com.appdirect.integration.vo.type.ErrorCode;
import com.appdirect.integration.vo.type.NoticeType;

@Component("subsNoticeExecutor")
public class SubscriptionNoticeExecutor implements EventExecutor {

	private static final Logger logger = LogManager.getLogger(SubscriptionNoticeExecutor.class);

	@Autowired
	private AccountService accountService;

	@Autowired
	@Qualifier("subsCancleExecutor")
	private EventExecutor subsCancleExecutor;
	
	@Autowired
	@Qualifier("objectExistsValidator")
	private Validator objectExistsValidator;

	/**
	 * Subscription notice event executor
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public APIResponse executeEvent(EventInfo eventInfo) {

		String accountId = eventInfo.getPayload().getAccount().getAccountIdentifier();

		logger.info("Executing notification event for account :" + accountId);

		if (eventInfo.getPayload().getNotice().getType() == NoticeType.CLOSED) {
			logger.info("Account closed notification received for account :" + accountId);
			return subsCancleExecutor.executeEvent(eventInfo);
		} else if ((eventInfo.getPayload().getNotice().getType() == NoticeType.SUSPENDED)
				|| (eventInfo.getPayload().getNotice().getType() == NoticeType.DEACTIVATED)) {

			logger.info("Deactivating the account :" + accountId);

			Account account = accountService.getAccountById(accountId);
			if(!objectExistsValidator.validate(account)){
				return new APIResponse(false, "Account does not exists for account ID: " + accountId, ErrorCode.ACCOUNT_NOT_FOUND);
			}
			
			account.setIsActive(0);
			accountService.saveAccount(account);
			return new APIResponse(true, "Account deactivated successfully." + account.getId(), null, account.getId());
		} else {
			return new APIResponse(true, "Received notification which need not be handled....");
		}

	}

}
