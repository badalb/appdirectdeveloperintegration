package com.appdirect.integration.handler;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.appdirect.integration.eventexecutor.EventExecutor;
import com.appdirect.integration.vo.APIResponse;
import com.appdirect.integration.vo.EventInfo;
import com.appdirect.integration.vo.type.ErrorCode;

@Component
public class IntegrationEventHandlerImpl implements IntegrationEventHandler {

	private static final Logger logger = LogManager.getLogger(IntegrationEventHandlerImpl.class);

	@Autowired
	@Qualifier("subsCancleExecutor")
	private EventExecutor subsCancleExecutor;

	@Autowired
	@Qualifier("subsChangeExecutor")
	private EventExecutor subsChangeExecutor;

	@Autowired
	@Qualifier("subsNoticeExecutor")
	private EventExecutor subsNoticeExecutor;

	@Autowired
	@Qualifier("subsOrderExecutor")
	private EventExecutor subsOrderExecutor;

	@Autowired
	@Qualifier("userAssignmentExecutor")
	private EventExecutor userAssignmentExecutor;

	@Autowired
	@Qualifier("subsuserUnassignExecutor")
	private EventExecutor subsuserUnassignExecutor;

	/**
	 * Register event processor and call the appropriate executor from here
	 */
	@Override
	public APIResponse handleEvent(EventInfo eventInfo) {
		switch (eventInfo.getType()) {
		case SUBSCRIPTION_ORDER:
			logger.info("Processing SUBSCRIPTION_ORDER for event :" + eventInfo.getReturnUrl());
			return subsOrderExecutor.executeEvent(eventInfo);
		case SUBSCRIPTION_CHANGE:
			logger.info("Processing SUBSCRIPTION_CHANGE for event :" + eventInfo.getReturnUrl());
			return subsChangeExecutor.executeEvent(eventInfo);
		case SUBSCRIPTION_CANCEL:
			logger.info("Processing SUBSCRIPTION_CANCEL for event :" + eventInfo.getReturnUrl());
			return subsCancleExecutor.executeEvent(eventInfo);
		case USER_ASSIGNMENT:
			logger.info("Processing USER_ASSIGNMENT for event :" + eventInfo.getReturnUrl());
			return userAssignmentExecutor.executeEvent(eventInfo);
		case USER_UNASSIGNMENT:
			logger.info("Processing USER_UNASSIGNMENT for event :" + eventInfo.getReturnUrl());
			return subsuserUnassignExecutor.executeEvent(eventInfo);
		case SUBSCRIPTION_NOTICE:
			logger.info("Processing SUBSCRIPTION_NOTICE for event :" + eventInfo.getReturnUrl());
			return subsNoticeExecutor.executeEvent(eventInfo);
		default:
			return new APIResponse(false,
					"Event type not supported by this endpoint: " + String.valueOf(eventInfo.getType()),
					ErrorCode.UNKNOWN_ERROR);
		}
	}

}
