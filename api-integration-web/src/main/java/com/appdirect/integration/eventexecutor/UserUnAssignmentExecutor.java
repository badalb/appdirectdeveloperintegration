package com.appdirect.integration.eventexecutor;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.appdirect.integration.domain.Account;
import com.appdirect.integration.domain.User;
import com.appdirect.integration.handler.IntegrationEventHandlerImpl;
import com.appdirect.integration.service.AccountService;
import com.appdirect.integration.service.UserService;
import com.appdirect.integration.validator.Validator;
import com.appdirect.integration.vo.APIResponse;
import com.appdirect.integration.vo.EventInfo;
import com.appdirect.integration.vo.type.ErrorCode;

@Component("subsuserUnassignExecutor")
public class UserUnAssignmentExecutor implements EventExecutor {

	private static final Logger logger = LogManager.getLogger(IntegrationEventHandlerImpl.class);

	@Autowired
	private AccountService accountService;

	@Autowired
	private UserService userService;
	
	@Autowired
	@Qualifier("objectExistsValidator")
	private Validator objectExistsValidator;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public APIResponse executeEvent(EventInfo eventInfo) {

		String appDirectUuid = eventInfo.getPayload().getUser().getUuid();
		String accountId = eventInfo.getPayload().getAccount().getAccountIdentifier();
		logger.info("Unassigning user :" + appDirectUuid + " from account :" + accountId);

		User user = userService.findUserByUuid(appDirectUuid);
		
		if(!objectExistsValidator.validate(user)){
			return new APIResponse(false, "User does not exists for this account", ErrorCode.USER_NOT_FOUND);
		}
		
		Account account = accountService.getAccountById(accountId);
		
		if(!objectExistsValidator.validate(account)){
			return new APIResponse(false, "Account does not exists for this account", ErrorCode.ACCOUNT_NOT_FOUND);
		}

		List<User> users = account.getUser();
		users.remove(user);

		account.setUser(users);
		accountService.saveAccount(account);
		logger.info("User :" + appDirectUuid + " unassigned from account :" + accountId);

		return new APIResponse(true, "User unassigned successfully", null, accountId);

	}

}
