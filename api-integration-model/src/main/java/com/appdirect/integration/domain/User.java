package com.appdirect.integration.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "user")
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6296104452969421227L;
	@Id
	@GenericGenerator(name = "appdirect_id_gen", strategy = "com.appdirect.integration.util.IDGenerator")
	@GeneratedValue(generator = "appdirect_id_gen")
	@Column(name = "id")
	private String id;
	
	private String email;

	private String firstName;

	private String language;

	private String lastName;

	private String openId;

	private String uuid;
	
	private boolean isAdmin;
	
	@OneToOne
	@JsonIgnore
	@JoinColumn(name="address_id_fk")
	private Address address;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "account_id_fk")
	private Account account;

	
	
	public User() {
		super();
	}
	
	public User(String email, String firstName, String language, String lastName, String openId, String uuid,
			boolean isAdmin, Address address, Account account) {
		super();
		this.email = email;
		this.firstName = firstName;
		this.language = language;
		this.lastName = lastName;
		this.openId = openId;
		this.uuid = uuid;
		this.isAdmin = isAdmin;
		this.address = address;
		this.account = account;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
}
