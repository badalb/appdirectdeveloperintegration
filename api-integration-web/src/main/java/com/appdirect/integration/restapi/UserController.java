package com.appdirect.integration.restapi;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.integration.config.ViewMapper;
import com.appdirect.integration.domain.User;
import com.appdirect.integration.restapi.view.UserVO;
import com.appdirect.integration.service.UserService;

@RestController
@RequestMapping({ "/api/v1", "" })
public class UserController {

	private static final Logger logger = LogManager.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private ViewMapper<UserVO> userViewMapper;

	/**
	 * Get user by UUID
	 * @param uuid
	 * @return
	 */
	@RequestMapping(value = "{uuid}/user/", method = RequestMethod.GET)
	public @ResponseBody UserVO userByUuid(@PathVariable("uuid") String uuid) {
		logger.info("Finding user by UUID: " + uuid);
		User user = userService.findUserByUuid(uuid);
		return userViewMapper.map(user, UserVO.class);
	}

	/**
	 * Get User by openID
	 * @param openId
	 * @return
	 */
	@RequestMapping(value = "/user/{openId}", method = RequestMethod.GET)
	public @ResponseBody UserVO findUserByOpenId(@PathVariable("openId") String openId) {
		logger.info("Finding user by openID: " + openId);
		User user = userService.findUserByUuid(openId);
		return userViewMapper.map(user, UserVO.class);
	}

	/**
	 * Get list of users
	 * @return
	 */
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public @ResponseBody List<UserVO> findAllUsers() {
		logger.info("Finding all users ..");

		List<User> users = userService.findAllUsers();
		List<UserVO> userVOList = new ArrayList<>();
		for (User user : users) {
			userVOList.add(userViewMapper.map(user, UserVO.class));
		}
		return userVOList;
	}

}
