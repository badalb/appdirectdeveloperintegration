package com.appdirect.integration.config;

public class AppDirectClientSecret {

	private String consumerKey;

	private String secret;

	public AppDirectClientSecret(String consumerKey, String secret) {
		this.consumerKey = consumerKey;
		this.secret = secret;
	}

	public String getConsumerKey() {
		return consumerKey;
	}

	public String getSecret() {
		return secret;
	}

}
