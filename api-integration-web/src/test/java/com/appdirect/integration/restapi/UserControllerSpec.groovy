package com.appdirect.integration.restapi

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

import spock.lang.Specification
import spock.lang.Stepwise

import com.appdirect.Application
import com.appdirect.integration.domain.User
import com.appdirect.integration.restapi.view.UserVO

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes
= [Application.class])@WebAppConfiguration
@IntegrationTest("server.port:8000")
@Stepwise
class UserControllerSpec extends Specification {

	@Value('${local.server.port}')
	int port;

	void"should save user"() {

		setup:
		def restTemplate = new RestTemplate()
		def userVO = new UserVO()
		userVO.uuid = 'dce4cce5-378b-47f3-81b3-56cf478c9c24'
		userVO.openId = '1234rty'
		userVO.email = 'test@test.com'
		userVO.firstName = 'Badal'
		userVO.lastName = 'Baidya'
		when:


		String uri="http://localhost:$port/api/v1/user/create";
		def user=restTemplate.postForObject(uri, userVO, User.class);


		then:
		user.uuid == userVO.uuid
	}

	void"find user by uuid"() {

		setup:
		def restTemplate = new RestTemplate()
		def uuid = 'dce4cce5-378b-47f3-81b3-56cf478c9c24'
		when:

		String uri="http://localhost:$port/api/v1/$uuid/user/";
		def user=restTemplate.getForObject(uri, User.class);



		then:
		user.uuid == 'dce4cce5-378b-47f3-81b3-56cf478c9c24'
	}

	void"find user by openId"() {

		setup:
		def restTemplate = new RestTemplate()
		def openId = 'dce4cce5-378b-47f3-81b3-56cf478c9c24'
		when:

		String uri="http://localhost:$port/api/v1/user/$openId";
		def user=restTemplate.getForObject(uri, User.class);



		then:
		user.uuid == 'dce4cce5-378b-47f3-81b3-56cf478c9c24'
	}

	void"find all users"() {

		setup:
		def restTemplate = new RestTemplate()
		when:

		String uri="http://localhost:$port/api/v1/users";
		def users=restTemplate.getForObject(uri, ArrayList.class);



		then:
		def user = users[0]
		user.uuid == 'dce4cce5-378b-47f3-81b3-56cf478c9c24'
	}
}
