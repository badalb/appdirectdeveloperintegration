package com.appdirect.integration.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "order_item")
public class OrderLineItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1725830298017603497L;
	@Id
	@GenericGenerator(name = "appdirect_id_gen", strategy = "com.appdirect.integration.util.IDGenerator")
	@GeneratedValue(generator = "appdirect_id_gen")
	@Column(name = "id")
	private String id;

	private Integer quantity;

	private String unit;
	
	@ManyToOne
	@JoinColumn(name = "order_id_fk")
	private SubscriptionOrder order;

	public OrderLineItem() {
		super();
	}

	public OrderLineItem(Integer quantity, String unit, SubscriptionOrder order) {
		super();
		this.quantity = quantity;
		this.unit = unit;
		this.order = order;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public SubscriptionOrder getOrder() {
		return order;
	}

	public void setOrder(SubscriptionOrder order) {
		this.order = order;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderLineItem other = (OrderLineItem) obj;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		return true;
	}
	
	

}
