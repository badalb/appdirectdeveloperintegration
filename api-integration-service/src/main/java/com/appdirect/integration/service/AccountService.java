package com.appdirect.integration.service;

import java.util.List;

import com.appdirect.integration.domain.Account;

public interface AccountService {

	public Account getAccountById(String accountId);

	public void deleteAccount(Account account);

	public Account saveAccount(Account account);

	public void deleteAccount(String accountId);

	public List<Account> getAllAccount();

	// TODO:delete

	public Account findByTestId(String testId);

}
