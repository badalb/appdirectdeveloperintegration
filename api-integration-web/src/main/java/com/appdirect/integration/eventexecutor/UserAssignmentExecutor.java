package com.appdirect.integration.eventexecutor;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.appdirect.integration.domain.Account;
import com.appdirect.integration.domain.Address;
import com.appdirect.integration.domain.User;
import com.appdirect.integration.service.AccountService;
import com.appdirect.integration.vo.APIResponse;
import com.appdirect.integration.vo.EventInfo;
import com.appdirect.integration.vo.type.ErrorCode;

@Component("userAssignmentExecutor")
public class UserAssignmentExecutor implements EventExecutor {

	private static final Logger logger = LogManager.getLogger(UserAssignmentExecutor.class);

	@Autowired
	private AccountService accountService;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public APIResponse executeEvent(EventInfo eventInfo) {

		String accountId = eventInfo.getPayload().getAccount().getAccountIdentifier();

		logger.info("Processing user assignment event for account: " + accountId);
		Account account = accountService.getAccountById(accountId);
		List<User> users = account.getUser();

		String userUuid = eventInfo.getPayload().getUser().getUuid();

		for (User user : users) {
			if (user.getUuid().equalsIgnoreCase(userUuid)) {
				return new APIResponse(false, "User Already subscribed!!", ErrorCode.USER_ALREADY_EXISTS);
			}
		}

		logger.info("Assigning a new user for account: " + accountId);
		User user = new User();
		user.setAdmin(false);
		try {
			BeanUtils.copyProperties(user, eventInfo.getPayload().getUser());
			Address address = new Address();
			if (eventInfo.getCreator().getAddress() != null) {
				BeanUtils.copyProperties(address, eventInfo.getCreator().getAddress());
				user.setAddress(address);
			}
		} catch (IllegalAccessException e) {
			logger.debug("Error processing bean data for subscription change please take care. Proceeding with execution");
		} catch (InvocationTargetException e) {
			logger.debug("Error processing bean data for subscription change please take care. Proceeding with execution");
		}

		user.setAdmin(false);
		user.setAccount(account);

		users.add(user);
		account.setUser(users);

		accountService.saveAccount(account);

		return new APIResponse(true, "User added successfully to the account", null, accountId);

	}

}
