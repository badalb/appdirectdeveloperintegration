package com.appdirect.integration.handler;

import com.appdirect.integration.vo.APIResponse;
import com.appdirect.integration.vo.EventInfo;

public interface IntegrationEventHandler {

	public APIResponse handleEvent(EventInfo event);
}
