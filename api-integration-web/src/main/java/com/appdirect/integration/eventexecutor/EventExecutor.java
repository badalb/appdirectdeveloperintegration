package com.appdirect.integration.eventexecutor;

import com.appdirect.integration.vo.APIResponse;
import com.appdirect.integration.vo.EventInfo;

public interface EventExecutor {

	public APIResponse executeEvent(EventInfo eventInfo);
}
