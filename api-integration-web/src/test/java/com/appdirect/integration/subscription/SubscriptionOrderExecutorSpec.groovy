package com.appdirect.integration.subscription

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration

import spock.lang.Specification
import spock.lang.Stepwise

import com.appdirect.Application
import com.appdirect.integration.handler.IntegrationEventHandler
import com.appdirect.integration.vo.EventInfo
import com.google.gson.Gson

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes
= [Application.class])@WebAppConfiguration
@IntegrationTest("server.port:8000")
@Stepwise
class SubscriptionOrderExecutorSpec extends Specification {

	@Value('${local.server.port}')
	int port;

	@Autowired
	IntegrationEventHandler integrationEventHandler;

	 def " should create subscription" (){
	 setup:
	
	// def jsonStr ="{\"type\":\"SUBSCRIPTION_ORDER\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www.appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\"2f4e9323-ad0d-4220-87c0-e75cdec7376e\",\"openId\":\"https://www.appdirect.com/openid/id/2f4e9323-ad0d-4220-87c0-e75cdec7376e\",\"email\":\"appdirectdemo.badal@gmail.com\",\"firstName\":\"Badal\",\"lastName\":\"Baidya\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"payload\":{\"user\":null,\"company\":{\"uuid\":\"5f2ce143-bbd6-4d79-8461-fddff76d524b\",\"externalId\":null,\"name\":\"selforg\",\"email\":null,\"phoneNumber\":null,\"website\":\"selforg.com\",\"country\":\"US\"},\"account\":null,\"addonInstance\":null,\"addonBinding\":null,\"order\":{\"editionCode\":\"ONE_TIME\",\"addonOfferingCode\":null,\"pricingDuration\":\"ONE_TIME\",\"items\":[{\"unit\":\"USER\",\"quantity\":1}]},\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}"
	 
	 def jsonStr = "{\"type\":\"SUBSCRIPTION_ORDER\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www.appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\"2f4e9323-ad0d-4220-87c0-e75cdec7376e\",\"openId\":\"https://www.appdirect.com/openid/id/2f4e9323-ad0d-4220-87c0-e75cdec7376e\",\"email\":\"appdirectdemo.badal@gmail.com\",\"firstName\":\"Badal\",\"lastName\":\"Baidya\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"payload\":{\"user\":null,\"company\":{\"uuid\":\"5f2ce143-bbd6-4d79-8461-fddff76d524b\",\"externalId\":null,\"name\":\"selforg\",\"email\":null,\"phoneNumber\":null,\"website\":\"selforg.com\",\"country\":\"US\"},\"account\":null,\"addonInstance\":null,\"addonBinding\":null,\"order\":{\"editionCode\":\"ONE_TIME\",\"addonOfferingCode\":null,\"pricingDuration\":\"ONE_TIME\",\"items\":[{\"unit\":\"USER\",\"quantity\":1}]},\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}"
	 def gson = new Gson()
	 def event = gson.fromJson(jsonStr, EventInfo.class)
	
	 when:
	
	 def apiResponse = integrationEventHandler.handleEvent(event)
	
	 then:
	 apiResponse.accountIdentifier != null
	 }
}
