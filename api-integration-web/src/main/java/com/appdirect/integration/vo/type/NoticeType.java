package com.appdirect.integration.vo.type;

public enum NoticeType {

	DEACTIVATED, REACTIVATED, CLOSED, UPCOMING_INVOICE, SUSPENDED;
}
