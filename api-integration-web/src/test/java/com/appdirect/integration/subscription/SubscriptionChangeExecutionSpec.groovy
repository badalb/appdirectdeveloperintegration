package com.appdirect.integration.subscription

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration

import spock.lang.Specification
import spock.lang.Stepwise

import com.appdirect.Application
import com.appdirect.integration.handler.IntegrationEventHandler
import com.appdirect.integration.vo.EventInfo
import com.google.gson.Gson

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes
= [Application.class])@WebAppConfiguration
@IntegrationTest("server.port:8000")
@Stepwise
class SubscriptionChangeExecutionSpec extends Specification {


	@Value('${local.server.port}')
	int port;

	@Autowired
	IntegrationEventHandler integrationEventHandler;

	def " should update subscription" (){
		setup:

		def jsonStr = "\n{\"type\":\"SUBSCRIPTION_CHANGE\",\"marketplace\":{\"partner\":\"ACME\",\"baseUrl\":\"https://acme.appdirect.com\"},\"applicationUuid\":null,\"flag\":\"STATELESS\",\"creator\":{\"uuid\":\"ec5d8eda-5cec-444d-9e30-125b6e4b67e2\",\"openId\":\"https://www.appdirect.com/openid/id/ec5d8eda-5cec-444d-9e30-125b6e4b67e2\",\"email\":\"test-email+creator@appdirect.com\",\"firstName\":\"DummyCreatorFirst\",\"lastName\":\"DummyCreatorLast\",\"language\":\"fr\",\"address\":null,\"attributes\":null},\"payload\":{\"user\":null,\"company\":null,\"account\":{\"accountIdentifier\":\"8835e97c-ce3d-411c-83db-21c1060a77ac\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":{\"editionCode\":\"PREMIUM\",\"addonOfferingCode\":null,\"pricingDuration\":\"MONTHLY\",\"items\":[{\"unit\":\"USER\",\"quantity\":20},{\"unit\":\"MEGABYTE\",\"quantity\":15}]},\"notice\":null,\"configuration\":{}},\"returnUrl\":\"https://www.appdirect.com/finishprocure?token=dummyChange\",\"links\":[]}"
		def gson = new Gson()
		def event = gson.fromJson(jsonStr, EventInfo.class)

		when:

		def apiResponse = integrationEventHandler.handleEvent(event)

		then:
		apiResponse.accountIdentifier != null
	}
}
