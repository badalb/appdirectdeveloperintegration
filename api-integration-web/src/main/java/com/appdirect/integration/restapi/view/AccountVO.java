package com.appdirect.integration.restapi.view;

public class AccountVO {

	private String uuid;

	private String appDirectBaseUrl;

	private String editionCode;

	private Integer maxUsers;

	private Integer isActive;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getAppDirectBaseUrl() {
		return appDirectBaseUrl;
	}

	public void setAppDirectBaseUrl(String appDirectBaseUrl) {
		this.appDirectBaseUrl = appDirectBaseUrl;
	}

	public String getEditionCode() {
		return editionCode;
	}

	public void setEditionCode(String editionCode) {
		this.editionCode = editionCode;
	}

	public Integer getMaxUsers() {
		return maxUsers;
	}

	public void setMaxUsers(Integer maxUsers) {
		this.maxUsers = maxUsers;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

}
