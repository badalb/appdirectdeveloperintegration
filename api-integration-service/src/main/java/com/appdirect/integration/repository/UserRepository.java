package com.appdirect.integration.repository;

import org.springframework.data.repository.CrudRepository;

import com.appdirect.integration.domain.User;

public interface UserRepository extends CrudRepository<User, String> {

	public User findByOpenId(String openId);

	public User findByUuid(String uuid);
}
