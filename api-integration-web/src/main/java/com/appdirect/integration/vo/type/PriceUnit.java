package com.appdirect.integration.vo.type;

public enum PriceUnit {
	USER, GIGABYTE, MEGABYTE, PROPERTY, UNIT, ROOM, HOST, PROVIDER, MANAGER, NOT_APPLICABLE;
}
