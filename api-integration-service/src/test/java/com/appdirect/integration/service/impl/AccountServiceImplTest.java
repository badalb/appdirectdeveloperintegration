package com.appdirect.integration.service.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.appdirect.integration.domain.Account;
import com.appdirect.integration.repository.AccountRepository;
import com.appdirect.integration.service.builder.TestAccountBuilder;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

	@Mock
	private AccountRepository accountRepository;

	@InjectMocks
	private AccountServiceImpl accountServiceImpl;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetAccountById() {
		String accountId = "ec5d8eda-5cec-444d-9e30-125b6e4b67e2";
		Account account = TestAccountBuilder.buldAccount();

		when(accountServiceImpl.getAccountById(accountId)).thenReturn(account);

		Account result = accountServiceImpl.getAccountById(accountId);

		// API invocation result & data validation
		Assert.assertTrue(result != null);
		Assert.assertEquals(result.getId(), accountId);
	}

	@Test
	public void testSaveAccount() {
		String accountId = "ec5d8eda-5cec-444d-9e30-125b6e4b67e2";
		Account account = TestAccountBuilder.buldAccount();

		when(accountServiceImpl.saveAccount(account)).thenReturn(account);

		Account result = accountServiceImpl.saveAccount(account);

		// API was called exactly once
		verify(accountRepository, times(1)).save(account);
		// API invocation result & data validation
		Assert.assertTrue(result != null);
		Assert.assertEquals(result.getId(), accountId);
	}

	@Test
	public void testGetAllAccounts() {
		String accountId = "ec5d8eda-5cec-444d-9e30-125b6e4b67e2";
		Account account = TestAccountBuilder.buldAccount();

		List<Account> accounts = new ArrayList<>();
		accounts.add(account);

		when(accountServiceImpl.getAllAccount()).thenReturn(accounts);

		List<Account> resultList = accountServiceImpl.getAllAccount();

		// API was called exactly once
		verify(accountRepository, times(1)).findAll();
		// API invocation result & data validation
		Assert.assertTrue(resultList != null);
		Assert.assertEquals(resultList.get(0).getId(), accountId);

	}

	@Test
	public void testDeleteAccount() {

		Account account = TestAccountBuilder.buldAccount();

		accountServiceImpl.deleteAccount(account);

		// API was called exactly once
		verify(accountRepository, times(1)).delete(account);
		;

	}

}
