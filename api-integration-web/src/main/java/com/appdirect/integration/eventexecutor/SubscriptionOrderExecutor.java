package com.appdirect.integration.eventexecutor;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.appdirect.integration.domain.Account;
import com.appdirect.integration.domain.Address;

import com.appdirect.integration.domain.OrderLineItem;
import com.appdirect.integration.domain.SubscriptionOrder;
import com.appdirect.integration.domain.User;
import com.appdirect.integration.service.AccountService;
import com.appdirect.integration.validator.Validator;
import com.appdirect.integration.vo.APIResponse;
import com.appdirect.integration.vo.EventInfo;
import com.appdirect.integration.vo.type.ErrorCode;

@Component("subsOrderExecutor")
public class SubscriptionOrderExecutor implements EventExecutor {
	
	private static final Logger logger = LogManager.getLogger(SubscriptionOrderExecutor.class);

	@Autowired
	private AccountService accountService;

	@Autowired
	@Qualifier("objectExistsValidator")
	private Validator objectExistsValidator;

	/**
	 * Subscription order event executor
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public APIResponse executeEvent(EventInfo eventInfo) {

		Account account = new Account();
		account.setAccountIdentifier(eventInfo.getPayload().getCompany().getUuid());
		// account.setTestIdentifier("dummy-account");

		Account existingAccount = accountService.getAccountById(eventInfo.getPayload().getCompany().getUuid());
		if (objectExistsValidator.validate(existingAccount)) {
			return new APIResponse(false,
					"Account already exists for account ID: " + eventInfo.getPayload().getCompany().getUuid(),
					ErrorCode.OPERATION_CANCELLED);
		}

		account.setAppDirectBaseUrl(eventInfo.getMarketplace().getBaseUrl());

		User adminUser = new User();
		adminUser.setAdmin(true);

		Address address = new Address();

		SubscriptionOrder order = new SubscriptionOrder();

		List<OrderLineItem> lineItems = new ArrayList<>();

		try {
			if (eventInfo.getPayload().getAccount() != null) {
				BeanUtils.copyProperties(account, eventInfo.getPayload().getAccount());
			}

			BeanUtils.copyProperties(adminUser, eventInfo.getCreator());

			if (eventInfo.getCreator().getAddress() != null) {
				BeanUtils.copyProperties(address, eventInfo.getCreator().getAddress());
				adminUser.setAddress(address);
			}

			BeanUtils.copyProperties(order, eventInfo.getPayload().getOrder());

			for (com.appdirect.integration.vo.OrderItem item : eventInfo.getPayload().getOrder().getItems()) {
				OrderLineItem orderItemTosave = new OrderLineItem();
				BeanUtils.copyProperties(orderItemTosave, item);
				orderItemTosave.setOrder(order);
				lineItems.add(orderItemTosave);
			}

		} catch (IllegalAccessException e) {
			logger.debug("Error processing bean data for subscription change please take care. Proceeding with execution");
		} catch (InvocationTargetException e) {
			logger.debug("Error processing bean data for subscription change please take care. Proceeding with execution now ");
		}

		// set order and order item
		List<SubscriptionOrder> orders = new ArrayList<>();

		order.setAccount(account);
		order.setOrderLineItem(lineItems);
		orders.add(order);

		account.setOrder(orders);

		// set user
		List<User> users = new ArrayList<User>();
		adminUser.setAccount(account);
		users.add(adminUser);
		account.setUser(users);

		Account savedAccount = accountService.saveAccount(account);

		return new APIResponse(true, "Account created successfully", null, savedAccount.getId());
	}

}
