package com.appdirect.integration.restapi;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.integration.handler.IntegrationEventHandler;
import com.appdirect.integration.oauthclient.SignedOauthClient;
import com.appdirect.integration.vo.APIResponse;
import com.appdirect.integration.vo.EventInfo;
import com.appdirect.integration.vo.type.ErrorCode;
import com.google.gson.Gson;

import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

@RestController
@RequestMapping("/appdirect/api/v1")
public class IntegrationController {

	private static final Logger logger = LogManager.getLogger(IntegrationController.class);

	@Autowired
	private SignedOauthClient signedOauthClient;

	@Autowired
	private IntegrationEventHandler integrationEventHandler;

	/**
	 * Single entry point for all incoming event notifications from appDirect
	 * API
	 * 
	 * @param request
	 * @param response
	 * @param eventUrl
	 * @return
	 */

	@RequestMapping("/event")
	public APIResponse processEvent(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "eventUrl", required = false) String eventUrl) {

		logger.info("Handling event with evetURL: " + eventUrl);

		String jsonResp = null;
		Gson gson = new Gson();
		try {
			//send a signed oauth request to get json payload from appdirect server
			jsonResp = signedOauthClient.signAndGetObject(eventUrl);
			logger.info("JSON response for the event with eventURL: " + eventUrl + " JSON:" + jsonResp);
		} catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException
				| IOException e) {
			logger.debug("Error occured while processing event with event URL: " + eventUrl + " Error Details :"
					+ e.getMessage());
			return new APIResponse(false, "Error occured while fetch event details from appDirect server for event URL:"
					+ eventUrl + " Error message: " + e.getMessage(), ErrorCode.CONFIGURATION_ERROR);
		}

		logger.info("Sending JSON response to handlers for further processing....");
		return integrationEventHandler.handleEvent(gson.fromJson(jsonResp, EventInfo.class));
	}
}
