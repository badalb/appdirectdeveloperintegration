package com.appdirect.integration.validator;

import org.springframework.stereotype.Component;

@Component("objectExistsValidator")
public class ObjectExistsValidator implements Validator {

	/**
	 * If object exists return true
	 */
	@Override
	public boolean validate(Object object) {
		return object != null ? true : false;
	}

}
