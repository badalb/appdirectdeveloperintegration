package com.appdirect.integration.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "subs_order")
public class SubscriptionOrder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6521003650753700091L;

	@Id
	@GenericGenerator(name = "appdirect_id_gen", strategy = "com.appdirect.integration.util.IDGenerator")
	@GeneratedValue(generator = "appdirect_id_gen")
	@Column(name = "id")
	private String id;

	private String editionCode;

	private String pricingDuration;

	@OneToMany(mappedBy = "order", orphanRemoval = true, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<OrderLineItem> orderLineItem;

	@ManyToOne
	@JoinColumn(name = "account_id_fk")
	private Account account;

	public SubscriptionOrder() {
		super();
	}

	
	public SubscriptionOrder(String editionCode, String pricingDuration, List<OrderLineItem> items, Account account) {
		super();
		this.editionCode = editionCode;
		this.pricingDuration = pricingDuration;
		this.orderLineItem = items;
		this.account = account;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEditionCode() {
		return editionCode;
	}

	public void setEditionCode(String editionCode) {
		this.editionCode = editionCode;
	}

	public String getPricingDuration() {
		return pricingDuration;
	}

	public void setPricingDuration(String pricingDuration) {
		this.pricingDuration = pricingDuration;
	}

	public List<OrderLineItem> getOrderLineItem() {
		return orderLineItem;
	}


	public void setOrderLineItem(List<OrderLineItem> orderSkews) {
		this.orderLineItem = orderSkews;
	}


	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((account == null) ? 0 : account.hashCode());
		result = prime * result + ((editionCode == null) ? 0 : editionCode.hashCode());
		result = prime * result + ((orderLineItem == null) ? 0 : orderLineItem.hashCode());
		result = prime * result + ((pricingDuration == null) ? 0 : pricingDuration.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubscriptionOrder other = (SubscriptionOrder) obj;
		if (account == null) {
			if (other.account != null)
				return false;
		} else if (!account.equals(other.account))
			return false;
		if (editionCode == null) {
			if (other.editionCode != null)
				return false;
		} else if (!editionCode.equals(other.editionCode))
			return false;
		if (orderLineItem == null) {
			if (other.orderLineItem != null)
				return false;
		} else if (!orderLineItem.equals(other.orderLineItem))
			return false;
		if (pricingDuration == null) {
			if (other.pricingDuration != null)
				return false;
		} else if (!pricingDuration.equals(other.pricingDuration))
			return false;
		return true;
	}

}
