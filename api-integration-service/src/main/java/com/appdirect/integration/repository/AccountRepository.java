package com.appdirect.integration.repository;

import org.springframework.data.repository.CrudRepository;

import com.appdirect.integration.domain.Account;

public interface AccountRepository extends CrudRepository<Account, String>{

	//TODO: for testing only remove it.
	public Account findByTestIdentifier(String testIdentifier);
}
