package com.appdirect.integration.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "account")
public class Account implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2252589112488544377L;
	
	@Id
	@GenericGenerator(name = "appdirect_id_gen", strategy = "com.appdirect.integration.util.IDGenerator")
	@GeneratedValue(generator = "appdirect_id_gen")
	@Column(name = "id")
	private String id;
	
	private String accountIdentifier;

	private String appDirectBaseUrl;
	
	private Integer isActive = 1;
	
	private String status;
	
	/**
	 * Ignore this column: added for testing
	 */
	private String testIdentifier;
	
	@JsonIgnore
	@OneToMany(mappedBy = "account", orphanRemoval = true, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<User> user;
	
	@JsonIgnore
	@OneToMany(mappedBy = "account", orphanRemoval = true, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<SubscriptionOrder> order;

	public Account() {
		super();
	}

	public Account(String uuid, String appDirectBaseUrl, Integer isActive, String status, String testIdentifier,
			List<User> users, List<SubscriptionOrder> order) {
		super();
		this.accountIdentifier = uuid;
		this.appDirectBaseUrl = appDirectBaseUrl;
		this.isActive = isActive;
		this.status = status;
		this.testIdentifier = testIdentifier;
		this.user = users;
		this.order = order;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String uuid) {
		this.accountIdentifier = uuid;
	}

	public String getAppDirectBaseUrl() {
		return appDirectBaseUrl;
	}

	public void setAppDirectBaseUrl(String appDirectBaseUrl) {
		this.appDirectBaseUrl = appDirectBaseUrl;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTestIdentifier() {
		return testIdentifier;
	}

	public void setTestIdentifier(String testIdentifier) {
		this.testIdentifier = testIdentifier;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUser(List<User> users) {
		this.user = users;
	}

	public List<SubscriptionOrder> getOrder() {
		return order;
	}

	public void setOrder(List<SubscriptionOrder> order) {
		this.order = order;
	}

	
}
