package com.appdirect.integration.validator;

public interface Validator {

	public boolean validate(Object object);
}
