package com.appdirect.integration.eventexecutor;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.appdirect.integration.domain.Account;
import com.appdirect.integration.handler.IntegrationEventHandlerImpl;
import com.appdirect.integration.service.AccountService;
import com.appdirect.integration.validator.Validator;
import com.appdirect.integration.vo.APIResponse;
import com.appdirect.integration.vo.EventInfo;
import com.appdirect.integration.vo.type.ErrorCode;

@Component("subsCancleExecutor")
public class SubscriptionCancleExecutor implements EventExecutor {

	private static final Logger logger = LogManager.getLogger(IntegrationEventHandlerImpl.class);

	@Autowired
	private AccountService accountService;
	
	@Autowired
	@Qualifier("objectExistsValidator")
	private Validator objectExistsValidator;

	/**
	 * Event subscription cancel order executor
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public APIResponse executeEvent(EventInfo eventInfo) {

		logger.info("Executing subscription cancel event for account :"
				+ eventInfo.getPayload().getAccount().getAccountIdentifier());

		String accountId = eventInfo.getPayload().getAccount().getAccountIdentifier();
		
		Account account = accountService.getAccountById(accountId);
		
		if(!objectExistsValidator.validate(account)){
			return new APIResponse(false, "Account doesnot exists :" + accountId, ErrorCode.ACCOUNT_NOT_FOUND);
		}
		
		accountService.deleteAccount(account);

		logger.info("Executing subscription cancel event for account :"
				+ eventInfo.getPayload().getAccount().getAccountIdentifier() + "completed successfully");

		return new APIResponse(true, "Subscription cancelled successfully for account :" + accountId, null, accountId);
	}

}
