package com.appdirect.integration.service.impl;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.appdirect.integration.domain.Account;
import com.appdirect.integration.repository.AccountRepository;
import com.appdirect.integration.service.AccountService;


@Service
public class AccountServiceImpl implements AccountService {

	private static final Logger logger = LogManager.getLogger(AccountServiceImpl.class);

	@Autowired
	private AccountRepository accountRepository;

	@Override
	@Transactional(readOnly = true)
	public Account getAccountById(String accountId) {
		logger.info("Fetching account for account ID: "+ accountId);
		return accountRepository.findOne(accountId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteAccount(Account account) {
		logger.info("Deleting account with account ID: "+ account.getId());
		accountRepository.delete(account);

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Account saveAccount(Account account) {
		return accountRepository.save(account);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteAccount(String accountId) {
		logger.info("Deleting account with account ID: "+ accountId);
		Account account = getAccountById(accountId);
		accountRepository.delete(account);
	}

	@Override
	public List<Account> getAllAccount() {
		return (List<Account>) accountRepository.findAll();
	}

	@Override
	public Account findByTestId(String testIdentifier) {
		return accountRepository.findByTestIdentifier(testIdentifier);
	}

}
