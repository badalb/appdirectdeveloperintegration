package com.appdirect.integration.service.builder;

import com.appdirect.integration.domain.User;

public class TestUserBuilder {

	public static User buildTestUser() {
		User user = new User();
		user.setAdmin(true);
		user.setEmail("appDirectTest@test.com");
		user.setFirstName("John");
		user.setLanguage("EN-US");
		user.setLastName("Doe");
		user.setOpenId("https://www.appdirect.com/openid/id/2f4e9323-ad0d-4220-87c0-e75cdec7376e");
		user.setUuid("ec5d8eda-5cec-444d-9e30-125b6e4b67e2");
		
		return user;
	}
}
